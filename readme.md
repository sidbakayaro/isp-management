# ISP and Cable management
This project is fullstack application which can be used by ISP for user management and subscription management.

## Features
- User management
- Plan management
- Management of modes for plans
- Subscription management
- SMS reminder to customers before 2 days
- Acknowledgement of payment to customers