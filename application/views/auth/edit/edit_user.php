<div class="container mt-3">
	<h3 class="text-center my-3">Edit User</h3>
	<div class="row my-3 justify-content-center">
		<div class="col-6">
			<div class="alert alert-warning alert-dismissible fade <?php echo $message !== null && $message !== '' ? 'show' : 'd-none'; ?>" role="alert">
				<?php echo $message;?>
				<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
			</div>
			<div class="card rounded-border">
				<div class="card-body">
					<?php echo form_open(uri_string());?>
						<div class="row">
							<div class="col-12">
								<div class="mb-3">
									<label for="<?php echo $first_name['name'];?>" class="form-label">First Name</label>
									<?php echo form_input(array_merge($first_name, array("class" => 'form-control')));?>
								</div> 
								<div class="mb-3">
									<label for="<?php echo $last_name['name'];?>" class="form-label">Last Name</label>
									<?php echo form_input(array_merge($last_name, array("class" => 'form-control')));?>
								</div>
								<div class="mb-3">
									<label for="<?php echo $phone['name'];?>" class="form-label">Phone</label>
									<?php echo form_input(array_merge($phone, array("class" => 'form-control')));?>
								</div>
								<div class="mb-3">
									<label for="<?php echo $password['name'];?>" class="form-label">Password (if changing password)</label>
									<?php echo form_input(array_merge($password, array("class" => 'form-control')));?>
								</div>
								<div class="mb-3">
									<label for="<?php echo $password_confirm['name'];?>" class="form-label">Confirm Password (if changing password)</label>
									<?php echo form_input(array_merge($password_confirm, array("class" => 'form-control')));?>
								</div>
								<div class="mb-3">
									<label for="<?php echo $phone['name'];?>" class="form-label">Group</label>
									<?php if ($this->ion_auth->is_admin()): ?>
										<?php foreach ($groups as $group):?>
											<div class="form-check">
												<label class="form-check-label">
												<input class="form-check-input" type="radio" name="groups[]" value="<?php echo $group['id'];?>" <?php echo (in_array($group, $currentGroups)) ? 'checked="checked"' : null; ?>>
												<?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
												</label>
											</div>
										<?php endforeach?>
									<?php endif ?>
								</div>
							</div>
							<?php echo form_hidden('id', $user->id);?>
							<?php echo form_hidden($csrf); ?>
						</div>
						<?php echo form_submit('submit', lang('edit_user_submit_btn'), array("class" => "btn primary-color"));?>
					<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>
