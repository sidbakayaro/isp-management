<div class="container">
	<h3 class="text-center my-3">Edit Mode</h3>
	<div class="row my-3 justify-content-center">
		<div class="col-6">
			<div class="alert alert-warning alert-dismissible fade <?php echo $message !== '' ? 'show' : 'd-none'; ?>" role="alert">
				<?php echo $message;?>
				<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
			</div>
			<div class="card rounded-border">
				<div class="card-body">
					<?php echo form_open(uri_string());?>
						<div class="row">
							<div class="col-12">
								<div class="mb-3">
									<label for="<?php echo $name['name'];?>" class="form-label">Plan name</label>
									<?php echo form_input(array_merge($name, array("class" => 'form-control')));?>
								</div>
								<div class="mb-3">
									<label for="status" class="form-label">Status</label>
									<?php echo form_dropdown('active', $status['fields'], $status['value'], array("class" => 'form-select', "id" => "status"));?>
								</div>
								<div class="d-grid">
									<?php echo form_submit('submit', 'Update', array("class" => "btn primary-color"));?>
								</div>
							</div>
					<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>