<div class="container mt-3">
	<h3 class="text-center my-3">Deactivate User</h3>
	<div class="row my-3 justify-content-center">
		<div class="col-5">
			<p><?php echo sprintf(lang('deactivate_subheading'), $user->{$identity}); ?></p>
			<div class="card rounded-border">
				<div class="card-body">
					<?php echo form_open("auth/deactivate/".$user->id);?>
						<div class="row">
							<div class="col-6 mb-3">
								<label for="yes" class="form-label">Yes</label>
								<input id="yes" class="form-input-check" type="radio" name="confirm" value="yes" checked="checked" />
							</div> 
							<div class="col-6 mb-3">
								<label for="no" class="form-label">No</label>
								<input id="no" class="form-input-check" type="radio" name="confirm" value="no"/>
							</div>
							<?php echo form_hidden($csrf); ?>
							<?php echo form_hidden(['id' => $user->id]); ?>
						</div>
						<div class="d-grid">
							<?php echo form_submit('submit', lang('deactivate_submit_btn'), array("class" => "btn primary-color"));?>
						</div>
					<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>
