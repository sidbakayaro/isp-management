<div class="container">
	<h3 class="text-center my-3">Edit Subscription</h3>
	<div class="row my-3 justify-content-center">
		<div class="col-6">
			<div class="alert alert-warning alert-dismissible fade <?php echo $message !== '' ? 'show' : 'd-none'; ?>" role="alert">
				<?php echo $message;?>
				<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
			</div>
			<div class="card rounded-border">
				<div class="card-body">
					<?php echo form_open(uri_string());?>
						<div class="row">
							<div class="col-12">
								<div class="mb-3">
									<label for="plan_id" class="form-label">Plan name</label>
									<?php echo form_dropdown('plan_id', $all_plans, $data->plan_name, array("class" => 'form-select', "id" => "plan_id"));?>
								</div>
								<div class="mb-3">
									<label for="<?php echo $amount['name'];?>" class="form-label">Amount</label>
									<?php echo form_input(array_merge($amount, array("class" => 'form-control')));?>
								</div>
								<div class="mb-3">
									<label for="<?php echo $start_date['name'];?>" class="form-label">Start Date</label>
									<?php echo form_input(array_merge($start_date, array("class" => 'form-control')));?>
								</div>
								<div class="mb-3">
									<label for="<?php echo $expiry_date['name'];?>" class="form-label">Expiry Date</label>
									<?php echo form_input(array_merge($expiry_date, array("class" => 'form-control')));?>
								</div>
								<div class="mb-3">
									<label for="status" class="form-label">Payment Status</label>
									<?php echo form_dropdown($status['name'], $status['fields'], $status['value'], array("class" => 'form-select'));?>
								</div>
								<div class="mb-3">
									<label for="<?php echo $payment_date['name'];?>" class="form-label">Payment Date</label>
									<?php echo form_input(array_merge($payment_date, array("class" => 'form-control')));?>
								</div>
								<div class="d-grid">
									<?php echo form_submit('submit', 'Update', array("class" => "btn primary-color"));?>
								</div>
							</div>
						</div>
					<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>