<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<!-- Google fonts -->
	<link rel="preconnect" href="https://fonts.gstatic.com" />
	<link
		href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600&display=swap"
		rel="stylesheet"
	/>

	<!-- Bootstrap main css -->
	<link
		href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css"
		rel="stylesheet"
		integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x"
		crossorigin="anonymous"
	/>

	<!-- Datatable css -->
	<link
		rel="stylesheet"
		href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap5.min.css"
	/>

	<!-- Google font -->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;600&display=swap" rel="stylesheet">

	<!-- Custom css -->
	<link rel="stylesheet" href="<?php echo asset_url();?>/css/style.css">

	<!-- Custom js -->
	<!-- <script defer src="main.js"></script> -->
	<!-- Jquery cdn -->
	<script src="https://code.jquery.com/jquery-3.5.1.js"></script>

	<title>Mrinal Cable</title>
</head>
<body>		

	<div class="container">
		<h1 class="text-center my-3"><?php echo lang('login_heading');?></h1>
		<div class="row h-100 justify-content-center align-items-center">
			<div class="col-12 col-md-6 col-lg-6 d-flex mx-auto">
				<div class="card">
					<div class="card-body rounded-border">
						<div class="alert alert-warning alert-dismissible fade <?php echo $message ? "show" : "hide" ?>" role="alert">
							<?php echo $message;?>
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
						</div>
						<?php echo form_open("auth/login");?>
								<div class="mx-auto">
									<div class="row">
										<div class="col-12">
											<div class="mb-3">
												<label for="<?php echo $identity['name'];?>" class="form-label">Username</label>
												<?php echo form_input(array_merge($identity, array("class" => 'form-control')));?>
											</div>
										</div>
										<div class="col-12">
											<div class="mb-3">
												<label for="<?php echo $password['name'];?>" class="form-label">Password</label>
												<?php echo form_input(array_merge($password, array("class" => 'form-control')));?>
											</div>
										</div>
										<p>
							<?php echo lang('login_remember_label', 'remember');?>
							<?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
						</p>


						<p><?php echo form_submit('submit', lang('login_submit_btn'));?></p>
						<p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>
									</div>
								</div>
							</div>
						<?php echo form_close();?>
					</div>
				</div>
			</div>
		</div>
	</div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap5.min.js"></script>
</body>
</html>