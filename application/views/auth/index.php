<div class="container">
	<h1 class="text-center my-3">All Users</h1>
	<div class="row mt-3">
		<div class="col-12">
			<div class="p-md-3 p-2 bg-white rounded-border">
				<div class="data-table">
					<table
						id="userTable"
						class="table table-striped dt-responsive nowrap"
						style="width: 100%"
					>
						<thead>
							<tr class="table-headers">
								<th>ID</th>
								<th>Username</th>
								<th>Name</th>
								<th>Email</th>
								<th>Phone</th>
								<th><?php echo lang('index_groups_th');?></th>
								<th><?php echo lang('index_status_th');?></th>
								<th><?php echo lang('index_action_th');?></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<div class="d-flex my-3 justify-content-center">
				<a class="btn primary-color mx-2" href="auth/create_user">
					<?php echo lang('index_create_user_link') ?>
				</a>
				<a class="btn primary-color mx-2" href="auth/create_group">
					<?php echo lang('index_create_group_link') ?>
				</a>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function () {
		let table = $('#userTable').DataTable({
			'processing': true,
			'serverSide': true,
			'serverMethod': 'post',
			'pageLength': 10,
			'responsive': true,
			'ajax': {
				'url':'<?php echo base_url('auth/customerlist');?>',
			},
			'columns': [
				{ data: 'id' },
				{ data: 'username' },
				{ data: 'name' },
				{ data: 'email' },
				{ data: 'phone' },
				{ 
					data: 'groups',
					render: function ( data ) {
						return '<a class="nav-link" href="<?php echo base_url('auth/edit_group/')?>'+data[1]+'">'+data[0]+'</a>';
					} 
				},
				{ 
					data: 'active',
					render: function ( data ) {
						if (data[0] === '1') {
							return '<a class="link-success" href="<?php echo base_url('auth/deactivate/')?>'+data[1]+'">Active</a>';
						}
						else {
							return '<a class="link-danger" href="<?php echo base_url('auth/activate/')?>'+data[1]+'">Inactive</a>';
						}
					} 
				},
				{
					data: 'id',
					render: function (data) {
						return '<a class="btn primary-color" href="<?php echo base_url('auth/edit_user/')?>'+data+'">Edit</a>';
					}
				}
			]
		});
	});
</script>

