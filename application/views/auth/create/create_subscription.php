<div class="container">
	<h3 class="text-center my-3">Create Subscription</h3>
	<div class="row my-3 justify-content-center">
		<div class="col-6">
			<div class="alert alert-warning alert-dismissible fade <?php echo $message !== null && $message !== '' ? 'show' : 'd-none'; ?>" role="alert">
				<?php echo $message;?>
				<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
			</div>
			<div class="card rounded-border">
				<div class="card-body">
					<?php echo form_open(uri_string());?>
						<div class="row">
							<div class="col-12">
								<div class="mb-3">
									<label for="<?php echo $username['name'];?>" class="form-label">Username</label>
									<?php echo form_input(array_merge($username, array("class" => 'form-control')));?>
								</div>
								<div class="mb-3">
									<label for="<?php echo $type['name'];?>" class="form-label">Subscription Type</label>
									<?php echo form_dropdown($type['name'], $type['fields'], $type['value'], array("class" => 'form-select', "id" => "type"));?>
								</div>
								<div class="mb-3">
									<label for="mode_id" class="form-label">Subscription Mode</label>
									<?php echo form_dropdown('mode_id', $all_modes, 'default', array("class" => 'form-select', "id" => "mode_id"));?>
								</div>
								<div class="mb-3">
									<label for="plan_id" class="form-label">Plan</label>
									<select id="plan_id" name="plan_id" class="form-select">
										<option selected>Choose plan</option>
									</select>
								</div>
								<div class="mb-3">
									<label for="<?php echo $start['name'];?>" class="form-label">Start Date</label>
									<?php echo form_input(array_merge($start, array("class" => 'form-control')));?>
								</div>
								<div class="mb-3">
									<label for="<?php echo $expiry['name'];?>" class="form-label">Expiry Date</label>
									<?php echo form_input(array_merge($expiry, array("class" => 'form-control')));?>
								</div>
								<div class="mb-3">
									<label for="status" class="form-label">Payment Status</label>
									<?php echo form_dropdown($status['name'], $status['fields'], $status['value'], array("class" => 'form-select'));?>
								</div>
								<div class="mb-3">
									<label for="<?php echo $amount['name'];?>" class="form-label">Amount</label>
									<?php echo form_input(array_merge($amount, array("class" => 'form-control')));?>
								</div>
								<div class="mb-3">
									<label for="<?php echo $payment_date['name'];?>" class="form-label">Payment Date</label>
									<?php echo form_input(array_merge($payment_date, array("class" => 'form-control')));?>
								</div>
								<div class="d-grid">
									<?php echo form_submit('submit', 'Create Subscription', array("class" => "btn primary-color"));?>
								</div>
							</div>
						</div>
					<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function () {
		// set expiry date depending on the mode
		// not done as mode's value can be anything
		// $('#start').change(() => {
		// 	let startDate = new Date($('#start').val());
		// 	let expiryDate = startDate.setDate(startDate.getDate() + 30);
		// 	expiryDate = new Date(expiryDate).toISOString().split('T')[0];
		// 	console.log(expiryDate);
		// });

		// change plans list depending on mode
		$('#mode_id').change(() => {
			let modeId = $('#mode_id').val();
			$.post('<?php echo base_url('plan/show_filtered/'); ?>'+modeId, { type: $('#type').val() } , (data, status) => {
				let options = [];
				let data_array = JSON.parse(data);
				if (data_array.length !== 0) {
					data_array.map((option) => {
						options += '<option value='+option.id+'>'+option.name+'</option>';	
					});
				}
				else {
					options += '<option>No plans</option>';
				}
				$('#plan_id').html(options);
			});
		});

		// reset modes when subscription type is changed
		// again api will be called on selecting mode
		$('#type').change(() => { $('#mode_id').prop('selectedIndex',0); });
	});
</script>

