<div class="container mt-3">
	<h3 class="text-center my-3">Create User</h3>
	<div class="row my-3 justify-content-center">
		<div class="col-8">
			<div class="alert alert-warning alert-dismissible fade <?php echo $message !== null && $message !== '' ? 'show' : 'd-none'; ?>" role="alert">
				<?php echo $message;?>
				<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
			</div>
			<div class="card rounded-border">
				<div class="card-body">
					<?php echo form_open("auth/create/create_user");?>
						<div class="row">
							<div class="col-6">
								<div class="mb-3">
									<label for="<?php echo $first_name['name'];?>" class="form-label">First Name</label>
									<?php echo form_input(array_merge($first_name, array("class" => 'form-control')));?>
								</div>  
								<div class="mb-3">
									<label for="<?php echo $identity['name'];?>" class="form-label">Username</label>
									<?php echo form_input(array_merge($identity, array("class" => 'form-control')));?>
								</div>  
								<div class="mb-3">
									<label for="<?php echo $password['name'];?>" class="form-label">Password</label>
									<?php echo form_input(array_merge($password, array("class" => 'form-control')));?>
								</div>
								<div class="mb-3">
									<label for="<?php echo $password_confirm['name'];?>" class="form-label">Confirm Password</label>
									<?php echo form_input(array_merge($password_confirm, array("class" => 'form-control')));?>
								</div>
							</div>
							<div class="col-6">
								<div class="mb-3">
									<label for="<?php echo $last_name['name'];?>" class="form-label">Last Name</label>
									<?php echo form_input(array_merge($last_name, array("class" => 'form-control')));?>
								</div>  
								<div class="mb-3">
									<label for="<?php echo $email['name'];?>" class="form-label">Email</label>
									<?php echo form_input(array_merge($email, array("class" => 'form-control')));?>
								</div>  
								<div class="mb-3">
									<label for="<?php echo $phone['name'];?>" class="form-label">Phone</label>
									<?php echo form_input(array_merge($phone, array("class" => 'form-control')));?>
								</div>
							</div>
						</div>
						<?php echo form_submit('submit', lang('create_user_submit_btn'), array("class" => "btn primary-color"));?>
					<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>

