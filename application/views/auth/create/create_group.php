<div class="container mt-3">
	<h3 class="text-center my-3">Create Group</h3>
	<div class="row my-3 justify-content-center">
		<div class="col-6">
			<div class="alert alert-warning alert-dismissible fade <?php echo $message !== null && $message !== '' ? 'show' : 'd-none'; ?>" role="alert">
				<?php echo $message;?>
				<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
			</div>
			<div class="card rounded-border">
				<div class="card-body">
					<?php echo form_open(uri_string());?>
						<div class="row">
							<div class="col-12">
								<div class="mb-3">
									<label for="<?php echo $group_name['name'];?>" class="form-label">Group Name</label>
									<?php echo form_input(array_merge($group_name, array("class" => 'form-control')));?>
								</div> 
								<div class="mb-3">
									<label for="<?php echo $description['name'];?>" class="form-label">Description</label>
									<?php echo form_input(array_merge($description, array("class" => 'form-control')));?>
								</div>
							</div>
						</div>
						<?php echo form_submit('submit', lang('edit_group_submit_btn'), array("class" => "btn primary-color"));?>
					<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>
</div>