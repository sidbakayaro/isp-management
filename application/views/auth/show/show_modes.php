<div class="container">
	<div class="d-flex justify-content-between my-3">
		<h3 class="fw-bold">Modes</h3>
		<a href="<?php echo base_url('plan/create_mode'); ?>" class="btn btn-primary">Create Mode</a>
	</div>
	<div class="row mt-3">
		<div class="col-12">
			<div class="p-md-3 p-2 bg-white rounded-border">
				<ul class="nav nav-tabs mb-3">
					<li class="nav-item mb-2">
						<button id="typeActive" class="btn btn-danger" value="0">Show inactive</button>
					</li>
				</ul>
				<div class="table-responsive data-table">
					<table
						id="planTable"
						class="table table-striped"
						style="width: 100%"
					>
						<thead>
							<tr class="table-headers">
								<th>ID</th>
								<th>Name</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script>

	$(document).ready(function () {
		let type = 1;
		let active = 1;
		
		$('#typeInternet').click(() => {
			type = $('#typeInternet').val();
			$('#typeCable').removeClass('active');
			$('#typeInternet').addClass('active');
			table.ajax.reload();
		});

		$('#typeCable').click(() => {
			type = $('#typeCable').val();
			$('#typeCable').addClass('active');
			$('#typeInternet').removeClass('active');
			table.ajax.reload();
		});

		$('#typeActive').click(() => {
			active = $('#typeActive').val();
			if ($('#typeActive').val() === '1') {
				$('#typeActive').val(0);
				$('#typeActive').addClass('btn-danger');
				$('#typeActive').removeClass('btn-success');
				$('#typeActive').text('Show inactive');
			} else {
				$('#typeActive').val(1);
				$('#typeActive').addClass('btn-success');
				$('#typeActive').removeClass('btn-danger');
				$('#typeActive').text('Show active');
			}
			table.ajax.reload();
		});

        var table = $('#planTable').DataTable({
			'processing': true,
			'serverSide': true,
			'serverMethod': 'post',
			'pageLength': 10,
			'responsive': true,
			'ajax': {
				'url':'<?php echo base_url('auth/modeslist');?>',
				'data': function(d) {
					d.type = type;
					d.active = active;
				}
			},
			'columns': [
				{ data: 'id' },
				{ data: 'name' },
				{ 
					data: 'status',
					render: function ( data ) {
						if (data === 'active') {
							return '<span class="badge bg-success">'+data+'</span>';
						}
						else {
							return '<span class="badge bg-danger">'+data+'</span>';
						}
					} 
				},
				{ 
                    data: 'id',
                    render: function ( data ) {
						const planType = type === 1 ? 'internet' : 'cable';
						return '<a class="btn primary-color" href="<?php echo base_url('plan/manage_mode/')?>'+data+'">Edit</a>';
					} 
                }
			]
		});
	});

</script>
