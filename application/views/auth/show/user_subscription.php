<div class="container">
	<div class="d-flex justify-content-between my-3">
		<h3 class="fw-bold"><?php echo $username."'s "; ?> Subscriptions</h3>
	</div>
	<div class="row mt-3">
		<div class="col-12">
			<div class="p-md-3 p-2 bg-white rounded-border">
				<ul class="nav nav-tabs mb-3">
					<li class="nav-item">
						<button id="typeInternet" class="nav-link active typeToggle" value="1">Internet Subscriptions</button>
					</li>
					<li class="nav-item">
						<button id="typeCable" class="nav-link typeToggle" value="0">Cable Subscriptions</button>
					</li>
					<li class="nav-item mx-3">
						<button id="typeActive" class="btn btn-danger" value="0">Show unpaid</button>
					</li>
				</ul>
				<div class="data-table">
					<table
						id="userTable"
						class="table table-striped dt-responsive nowrap"
						style="width: 100%"
					>
						<thead>
							<tr id='theader' class="table-headers">
								<th>ID</th>
								<th>Username</th>
								<th>Name</th>
								<th>Plan</th>
								<th>Start date</th>
								<th>Expiry date</th>
								<th>Amount</th>
								<th>Phone</th>
								<th>Status</th>
								<th>Payment date</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script>

	$(document).ready(function () {
		let type = 1;
		let active = 1;
		const isAdmin = '<?php echo $this->ion_auth->is_admin();?>';
		
		$('#typeInternet').click(() => {
			type = $('#typeInternet').val();
			$('#typeCable').removeClass('active');
			$('#typeInternet').addClass('active');
			table.ajax.reload();
		});

		$('#typeCable').click(() => {
			type = $('#typeCable').val();
			$('#typeCable').addClass('active');
			$('#typeInternet').removeClass('active');
			table.ajax.reload();
		});

		$('#typeActive').click(() => {
			active = $('#typeActive').val();
			if ($('#typeActive').val() === '1') {
				$('#typeActive').val(0);
				$('#typeActive').addClass('btn-danger');
				$('#typeActive').removeClass('btn-success');
				$('#typeActive').text('Show unpaid');
			} else {
				$('#typeActive').val(1);
				$('#typeActive').addClass('btn-success');
				$('#typeActive').removeClass('btn-danger');
				$('#typeActive').text('Show paid');
			}
			table.ajax.reload();
		});

        var table = $('#userTable').DataTable({
			'processing': true,
			'serverSide': true,
			'serverMethod': 'post',
			'pageLength': 10,
			'responsive': true,
			'ajax': {
				'url':'<?php echo base_url("auth/subslist/".$username);?>',
				'data': function(d) {
					d.type = type;
					d.active = active;
				}
			},
			'columns': [
				{ data: 'id' },
				{ data: 'username'},
				{ data: 'name' },
				{ data: 'plan_name' },
				{ data: 'start_date' },
				{ data: 'expiry_date' },
				{ data: 'amount' },
				{ data: 'phone' },
				{ 
					data: 'status',
					render: function ( data ) {
						if (data === 'paid') {
							return '<span class="badge bg-success">'+data+'</span>';
						}
						else {
							return '<span class="badge bg-danger">'+data+'</span>';
						}
					} 
				},
				{ data: 'payment_date' },
                { 
                    data: 'id',
                    render: function ( data ) {
						const planType = type === 1 ? 'internet' : 'cable';
						return '<a class="btn primary-color" href="<?php echo base_url('subscription/manage/')?>'+planType+'/'+data+'">Edit</a>';
					} 
                },
			]
		});

		if (isAdmin) {
			table.columns([0, 1, 2, 7, -1]).visible(true);
		} else {
			table.columns([0, 1, 2, 7, -1]).visible(false);
		}
	});

</script>