<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!-- Google fonts -->
		<link rel="preconnect" href="https://fonts.gstatic.com" />
		<link
			href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600&display=swap"
			rel="stylesheet"
		/>

		<!-- Bootstrap main css -->
		<link
			href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css"
			rel="stylesheet"
			integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x"
			crossorigin="anonymous"
		/>

		<!-- Datatable css -->
		<link
			rel="stylesheet"
			href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap5.min.css"
		/>
		<link
			rel="stylesheet"
			href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap5.min.css"
		/>

        <!-- Google font -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;600&display=swap" rel="stylesheet">

		<!-- Custom css -->
		<link rel="stylesheet" href="<?php echo asset_url();?>/css/style.css">

		<!-- Custom js -->
		<!-- <script defer src="main.js"></script> -->
		<!-- Jquery cdn -->
		<script src="https://code.jquery.com/jquery-3.5.1.js"></script>

		<title>Mrinal Cable</title>
	</head>
	<body>
		<nav class="navbar navbar-dark navbar-expand-md sticky-top py-3 nav-bg">
			<div class="container-fluid">
				<a class="navbar-brand mb-0 h1" href="#">Mrinal Cable</a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
				<div class="navbar-nav">
					<a class="nav-link <?php echo str_contains(uri_string(), 'dashboard') ? 'active' : ''; ?>" href="<?php echo base_url();?>">Dashboard</a>
					<a class="nav-link <?php echo str_contains(uri_string(), 'subscription') ? 'active' : ''; ?>" href="<?php echo base_url('auth/subscriptions');?>">Susbcriptions</a>
					<a class="nav-link <?php echo str_contains(uri_string(), 'plan') ? 'active' : ''; ?>" href="<?php echo base_url('auth/plans');?>">Plans</a>
					<a class="nav-link <?php echo str_contains(uri_string(), 'mode') ? 'active' : ''; ?>" href="<?php echo base_url('auth/modes');?>">Modes</a>
					<a class="nav-link" href="<?php echo base_url('auth/logout');?>">Logout</a>
				</div>
				</div>
			</div>
		</nav>
