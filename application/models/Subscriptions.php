<?php

class Subscriptions extends CI_Model {

    /**
	 * Creates a new subscription
	 *
     * @param int $plan_id
     * @param datetime $start_date
     * @param datetime $expiry_date
     * @param varchar $username
     * @param int $amount
     * @param tinyint $status 0:inactive 1:active
	 *
	 * @return int/bool
	 */
    public function create_subscription($plan_id, $start_date, $expiry_date, $username, $amount, $status, $payment_date = null)
    {
        // check if plan exists
        $check_plan = $this->db->get_where('plans', ['id' => $plan_id])->num_rows(); 
        if($check_plan === 0)
        {
            return FALSE;
        }

        // check if username exists
        $check_user = $this->db->get_where('users', ['username' => $username])->num_rows(); 
        if($check_user === 0)
        {
            return FALSE;
        }

        $conditions = array(
            'username' => $username,
            'start_date' => $start_date
        );

        $existing_sub = $this->db->get_where('subscriptions', $conditions)->num_rows();
		if($existing_sub !== 0)
		{
			return FALSE;
		}

        $data = array(
            'plan_id' => $plan_id,
            'start_date' => $start_date,
            'expiry_date' => $expiry_date,
            'username' => $username,
            'amount' => $amount,
            'status' => $status,
            'payment_date' => $payment_date
        );
        $this->db->insert('subscriptions', $data);
        $subscription_id = $this->db->insert_id('subscriptions'.'_id_seq');

        // return newly created plan id
        return $subscription_id;
    }

    /**
	 * show subscriptions
     * 
	 * @return array subscription data
	 */
    public function subs_table($data = null, $username = '')
    {
        $response = array();
        // Table related props

        $draw = ($data != null) ? $data['draw'] : null;
        $start = ($data != null) ? $data['start'] : null;
        $rowperpage = ($data != null) ? $data['length'] : null; // Rows display per page
        $columnIndex = ($data != null) ? $data['order'][0]['column'] : null; // Column index
        $columnName = ($data != null) ? $data['columns'][$columnIndex]['data'] : null; // Column name
        $columnSortOrder = ($data != null) ? $data['order'][0]['dir'] : null; // asc or desc
        $searchValue = ($data != null) ? $data['search']['value'] : null; // Search 
        $type = ($data != null) ? $data['type'] : 1;
        $active = ($data != null) ? $data['active'] : 1;

        // Search query
        $searchQuery = "";
        if($searchValue != ''){
            $searchQuery = " (
                u.username like '%".$searchValue."%' or 
                name like '%".$searchValue."%' or 
                u.phone like'%".$searchValue."%' or 
                s.start_date like'%".$searchValue."%' or
                s.expiry_date like'%".$searchValue."%' or
                s.payment_date like'%".$searchValue."%'
            ) ";
        }

        // Total number of records without filtering
        $this->db->select('count(*) as allcount');
        $this->db->from('users as u');
		if($username != '')
            $this->db->where('u.username', $username);
        $this->db->where('s.type', $type);
        $this->db->where('s.status', $active);
        $this->db->join('subscriptions as s', 'u.username = s.username');
        $this->db->join('plans as p', 's.plan_id = p.id');
        $this->db->order_by('s.start_date', 'desc');
        $records = $this->db->get()->result();
        $totalRecords = $records[0]->allcount;

        // Total number of filtered records
        $this->db->select('count(*) as allcount');
        if($searchQuery != '')
            $this->db->where($searchQuery);
		if($username != '')
            $this->db->where('u.username', $username);
        $this->db->where('s.type', $type);
        $this->db->where('s.status', $active);
        $this->db->from('users as u');
        $this->db->join('subscriptions as s', 'u.username = s.username');
        $this->db->join('plans as p', 's.plan_id = p.id');
        $this->db->order_by('s.start_date', 'desc');
        $records = $this->db->get()->result();
        $totalRecordwithFilter = $records[0]->allcount;


        $this->db->select('
            s.id, u.username, CONCAT(u.first_name, " ", u.last_name) as name,
            u.phone, p.name as plan_name, s.start_date, s.expiry_date,
            s.amount, s.status, s.payment_date
        ');
        if($searchQuery != '')
            $this->db->where($searchQuery);
		if($username != '')
            $this->db->where('u.username', $username);
        $this->db->where('s.type', $type);
        $this->db->where('s.status', $active);
        $this->db->from('users as u');
        $this->db->join('subscriptions as s', 'u.username = s.username');
        $this->db->join('plans as p', 's.plan_id = p.id');
        $this->db->order_by($columnName, $columnSortOrder);
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get()->result();

        $tabledata = array();

        foreach( $records as $record ){
            $tabledata[] = array(
                "id" => $record->id, 
                "username"=>$record->username,
                "name"=>$record->name,
                "phone"=>$record->phone,
                "plan_name"=>$record->plan_name,
                "start_date"=>$record->start_date,
                "expiry_date"=>$record->expiry_date,
                "amount"=>$record->amount,
                "status"=>$record->status ? 'paid' : 'unpaid',
                "payment_date"=>$record->payment_date,
            ); 
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $tabledata
        );

        return $response;
    }

    /**
	 * show individual subscriptions
     * 
     * @param string username of the user
	 * @return array subscription data
	 */
    public function show_sub($username)
    {
        $this->db->select('
            s.id, u.username, CONCAT(u.first_name, " ", u.last_name) as name,
            u.phone, p.name as plan_name, s.start_date, s.expiry_date,
            s.amount, s.status, s.payment_date
        ');
        $this->db->where('u.username', $username);
        $this->db->from('users as u');
        $this->db->join('subscriptions as s', 'u.username = s.username');
        $this->db->join('plans as p', 's.plan_id = p.id');
        $this->db->order_by('s.start_date', 'desc');
        $this->db->limit(15);
        $result = $this->db->get()->result_array();
        return $result;
    }

    /**
	 * show subscription entry
     * 
     * @param int subscription id
	 * @return array entry data
	 */
    public function show($id)
    {
        $this->db->select('
            s.id, u.username, CONCAT(u.first_name, " ", u.last_name) as name,
            u.phone, p.name as plan_name, s.start_date, s.expiry_date,
            s.amount, s.status, s.payment_date
        ');
        $this->db->where('s.id', $id);
        $this->db->from('users as u');
        $this->db->join('subscriptions as s', 'u.username = s.username');
        $this->db->join('plans as p', 's.plan_id = p.id');
        $this->db->order_by('s.start_date', 'desc');
        $result = $this->db->get()->row();
        return $result;
    }

}
