<?php


class User extends CI_Model{

    /**
    * show customers
    * 
    * @return array customer data
    */
    public function customers_table($data = null)
    {
        $response = array();
        // Table related props

        $draw = ($data != null) ? $data['draw'] : null;
        $start = ($data != null) ? $data['start'] : null;
        $rowperpage = ($data != null) ? $data['length'] : null; // Rows display per page
        $columnIndex = ($data != null) ? $data['order'][0]['column'] : null; // Column index
        $columnName = ($data != null) ? $data['columns'][$columnIndex]['data'] : null; // Column name
        $columnSortOrder = ($data != null) ? $data['order'][0]['dir'] : null; // asc or desc
        $searchValue = ($data != null) ? $data['search']['value'] : null; // Search 

        // Search query
        $searchQuery = "";
        if($searchValue != ''){
            $searchQuery = " (
                u.username like '%".$searchValue."%' or 
                name like '%".$searchValue."%' or 
                u.phone like'%".$searchValue."%' or 
                u.email like'%".$searchValue."%' or
                u.active like'%".$searchValue."%'
            ) ";
        }

        // Total number of records without filtering
        $this->db->select('count(*) as allcount');
        $this->db->from('users as u');
        $this->db->join('users_groups as g', 'u.id = g.user_id');
        $this->db->join('groups as role', 'g.group_id = role.id');
        $this->db->order_by('u.username', 'desc');
        $records = $this->db->get()->result();
        $totalRecords = $records[0]->allcount;

        // Total number of filtered records
        $this->db->select('count(*) as allcount');
        if($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->from('users as u');
        $this->db->join('users_groups as g', 'u.id = g.user_id');
        $this->db->join('groups as role', 'g.group_id = role.id');
        $this->db->order_by('u.username', 'desc');
        $records = $this->db->get()->result();
        $totalRecordwithFilter = $records[0]->allcount;


        $this->db->select('
            u.id, u.username, concat(first_name, " ", last_name) as name, 
            role.name as groups, g.group_id, u.phone, u. email, u.active
        ');
        if($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->from('users as u');
        $this->db->join('users_groups as g', 'u.id = g.user_id');
        $this->db->join('groups as role', 'g.group_id = role.id');
        $this->db->order_by($columnName, $columnSortOrder);
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get()->result();

        $tabledata = array();

        foreach( $records as $record ){
            $tabledata[] = array(
                "id" => $record->id, 
                "username"=>$record->username,
                "name"=>$record->name,
                "groups"=>[$record->groups, $record->group_id],
                "email"=>$record->email,
                "phone"=>$record->phone,
                "active"=>[$record->active, $record->id],
            ); 
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $tabledata
        );

        return $response;
    }
}