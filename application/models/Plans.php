<?php

class Plans extends CI_Model{

    /**
	 * Creates mode for the plans
	 *
	 * @param int $mode_id
	 * @param string name   names - monthly, quarterly, etc.
	 *
	 * @return int/bool
	 */
    public function create_plan_mode($data)
    {

        // deny if mode name is not passed
        if(!$data['name'])
		{
			return FALSE;
		}

        $existing_mode = $this->db->get_where('plans_mode', ['name' => $data['name']])->num_rows();
		if($existing_mode !== 0)
		{
			return FALSE;
		}

        $this->db->insert('plans_mode', $data);
        $mode_id = $this->db->insert_id('plans_mode'.'_id_seq');

        // return newly created mode id
        return $mode_id;
    }

    /**
	 * Creates a new plan
	 *
     * @param varchar $name
     * @param int $price
     * @param int $company_price
     * @param bool $status
     * @param int $mode_id
	 *
	 * @return int/bool
	 */
    public function create_plan($name, $price, $company_price, $status, $mode_id, $type)
    {

        // deny if mode name is not passed
        if(!$name)
		{
			return FALSE;
		}

        $existing_plan = $this->db->get_where('plans', ['name' => $name])->num_rows();
		if($existing_plan !== 0)
		{
			return FALSE;
		}

        $data = array(
            'name' => $name,
            'price' => $price,
            'company_price' => $company_price,
            'status' => $status,
            'mode_id' => $mode_id,
            'type' => $type
        );
        $this->db->insert('plans', $data);
        $plan_id = $this->db->insert_id('plans'.'_id_seq');

        // return newly created plan id
        return $plan_id;
    }

    /**
	 * Deactivate plan, mode, subscription
	 *
     * @param int $id
	 *
	 * @return bool
	 */
    public function deactivate($id, $type, $date = null)
    {
        // check if plan exists
        $check = $this->db->get_where($type, ['id' => $id])->num_rows(); 
        if($check === 0)
        {
            return FALSE;
        }

        if ($date)
        {
            $this->db->where('id', $id);
            $this->db->update($type, array('status' => 0, 'payment_date' => $date));
        }
        else
        {
            $this->db->where('id', $id);
            $this->db->update($type, array('status' => 0));
        }
        return TRUE;
    }

    /**
	 * Activate mode, plan, subscription
	 *
     * @param int $id
	 *
	 * @return bool
	 */
    public function activate($id, $type, $date = null)
    {
        // check if mode exists
        $check = $this->db->get_where($type, ['id' => $id])->num_rows(); 
        if($check === 0)
        {
            return FALSE;
        }

        if ($date)
        {
            $this->db->where('id', $id);
            $this->db->update($type, array('status' => 1, 'payment_date' => $date));
        }
        else
        {
            $this->db->where('id', $id);
            $this->db->update($type, array('status' => 1));
        }
        return TRUE;
    }

    /**
	 * show plans
     * 
     * @param int id - plan_id
     * @param string type - filter plans of type cable and internet - optional
	 * @return array plans data
	 */
    public function show_plans($id = null, $type = null)
    {
        $this->db->select('
            p.id, p.name as plan, p.price, 
            p.company_price, p.status, m.name as mode
        ');
        if ($id !== null)
            $this->db->where('p.id', $id);
        if ($type !== null)
        {
            if ($type === 'internet')
            {
                $this->db->where('p.type', 1);
            }
            else
            {
                $this->db->where('p.type', 0);
            }
        }
        $this->db->from('plans as p');
        $this->db->join('plans_mode as m', 'p.mode_id = m.id', 'left');
        $this->db->order_by('p.price', 'desc');
        $this->db->limit(15);
        $result = $this->db->get()->result_object();
        return $result;
    }
    

    /**
	 * show filtered plans
     * 
	 * @return array plans data
	 */
    public function show_filtered_plans($id)
    {
        $type = $this->input->post('type');
        $this->db->select('id, name');
        $this->db->where('mode_id', $id);
        $this->db->where('type', $type);
        $this->db->from('plans');
        $this->db->limit(15);
        $result = $this->db->get()->result_array();
        return $result;
    }

    /**
	 * show plans
     * 
	 * @return array plans table data
	 */
    public function plans_table($data = null)
    {
        $response = array();
        // Table related props

        $draw = ($data != null) ? $data['draw'] : null;
        $start = ($data != null) ? $data['start'] : null;
        $rowperpage = ($data != null) ? $data['length'] : null; // Rows display per page
        $columnIndex = ($data != null) ? $data['order'][0]['column'] : null; // Column index
        $columnName = ($data != null) ? $data['columns'][$columnIndex]['data'] : null; // Column name
        $columnSortOrder = ($data != null) ? $data['order'][0]['dir'] : null; // asc or desc
        $searchValue = ($data != null) ? $data['search']['value'] : null; // Search 
        $type = ($data != null) ? $data['type'] : 1;
        $active = ($data != null) ? $data['active'] : 1;

        // Search query
        $searchQuery = "";
        if($searchValue != ''){
            $searchQuery = " (
                p.name like '%".$searchValue."%' or 
                p.price like'%".$searchValue."%' or 
                p.company_price like'%".$searchValue."%' or
                p.status like'%".$searchValue."%' or
                m.name like'%".$searchValue."%'
            ) ";
        }

        // Total number of records without filtering
        $this->db->select('count(*) as allcount');
        $this->db->from('plans as p');
        $this->db->where('type', $type);
        $this->db->where('p.status', $active);
        $this->db->join('plans_mode as m', 'p.mode_id = m.id', 'left');
        $this->db->order_by('p.price', 'desc');
        $records = $this->db->get()->result();
        $totalRecords = $records[0]->allcount;

        // Total number of filtered records
        $this->db->select('count(*) as allcount');
        if($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->where('p.type', $type);
        $this->db->where('p.status', $active);
        $this->db->from('plans as p');
        $this->db->join('plans_mode as m', 'p.mode_id = m.id', 'left');
        $this->db->order_by('p.price', 'desc');
        $records = $this->db->get()->result();
        $totalRecordwithFilter = $records[0]->allcount;

        $this->db->select('
            p.id, p.name as plan, p.price, 
            p.company_price, p.status, m.name as mode
        ');
        if($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->where('p.type', $type);
        $this->db->where('p.status', $active);
        $this->db->from('plans as p');
        $this->db->join('plans_mode as m', 'p.mode_id = m.id', 'left');
        $this->db->order_by($columnName, $columnSortOrder);
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get()->result();

        $tabledata = array();

        foreach( $records as $record ){
            $tabledata[] = array(
                "id" => $record->id, 
                "plan"=>$record->plan,
                "price"=>$record->price,
                "company_price"=>$record->company_price,
                "status"=>$record->status ? 'active' : 'inactive',
                "mode"=>$record->mode,
            ); 
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $tabledata
        );

        return $response;
    }

    /**
	 * show modes
     * 
	 * @return array modes table data
	 */
    public function modes_table($data = null)
    {
        $response = array();
        // Table related props

        $draw = ($data != null) ? $data['draw'] : null;
        $start = ($data != null) ? $data['start'] : null;
        $rowperpage = ($data != null) ? $data['length'] : null; // Rows display per page
        $columnIndex = ($data != null) ? $data['order'][0]['column'] : null; // Column index
        $columnName = ($data != null) ? $data['columns'][$columnIndex]['data'] : null; // Column name
        $columnSortOrder = ($data != null) ? $data['order'][0]['dir'] : null; // asc or desc
        $searchValue = ($data != null) ? $data['search']['value'] : null; // Search 
        $active = ($data != null) ? $data['active'] : 1;

        // Search query
        $searchQuery = "";
        if($searchValue != ''){
            $searchQuery = "name like '%".$searchValue."%'";
        }

        // Total number of records without filtering
        $this->db->select('count(*) as allcount');
        $this->db->from('plans_mode');
        $this->db->where('status', $active);
        $this->db->order_by('id', 'desc');
        $records = $this->db->get()->result();
        $totalRecords = $records[0]->allcount;

        // Total number of filtered records
        $this->db->select('count(*) as allcount');
        if($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->from('plans_mode');
        $this->db->where('status', $active);
        $this->db->order_by('id', 'desc');
        $records = $this->db->get()->result();
        $totalRecordwithFilter = $records[0]->allcount;

        $this->db->select('*');
        if($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->from('plans_mode');
        $this->db->where('status', $active);
        $this->db->order_by($columnName, $columnSortOrder);
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get()->result();

        $tabledata = array();

        foreach( $records as $record ){
            $tabledata[] = array(
                "id" => $record->id, 
                "name"=>$record->name,
                "status"=>$record->status ? 'active' : 'inactive'
            ); 
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $tabledata
        );

        return $response;
    }

    /**
	 * show modes
     * 
	 * @return array modes data
	 */
    public function show_modes($id = null)
    {
        if ($id === null)
        {
            $result = $this->db->get_where('plans_mode', array('status' => 1), 15, 0)->result_object();
        }
        else
        {
            $result = $this->db->get_where('plans_mode', array('status' => 1, 'id' => $id), 15, 0)->result_object();
        }
        return $result;
    }

    /**
	 * update plans, modes
     * 
     * @param array data
     * 
	 * @return bool
	 */
    public function update($id, array $data, $type)
    {
        $plan = $this->db->get_where($type, array('id' => $id))->row();
        if($plan === null)
        {
            return FALSE;
        }
        
        $this->db->update($type, $data, array('id' => $id));
        return TRUE;
    }


}