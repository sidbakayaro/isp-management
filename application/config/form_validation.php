<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['error_prefix'] = '<div class="alert alert-warning alert-dismissible fade show" role="alert"><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>';
$config['error_suffix'] = '</div>';