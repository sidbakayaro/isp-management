<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Subscription extends CI_Controller
{
    public $data = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(['ion_auth', 'form_validation', 'user_agent']);
		$this->load->helper(['url', 'language']);

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');

		$this->load->model('User');
		$this->load->model('Plans');
		$this->load->model('Subscriptions');

		$this->userModel = new User;
		$this->plans_model = new Plans;
		$this->subs_model = new Subscriptions;
	}

	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}

	public function show($id)
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth', 'refresh');
		}

		$response = $this->subs_model->show($id);
		echo $response;
	}

	// create new subscription
	public function subscribe()
	{
		if (!$this->ion_auth->logged_in() && !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		// validate form input
		$this->form_validation->set_rules('plan_id', 'Plan name', 'trim|required|numeric');
		$this->form_validation->set_rules('start', 'Start date', 'trim|required|valid_date');
		$this->form_validation->set_rules('expiry', 'Expiry date', 'trim|required|valid_date');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_dash');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required|numeric');
		$this->form_validation->set_rules('active', 'Status', 'trim|required|max_length[1]');
		$this->form_validation->set_rules('payment', 'Payment date', 'trim|valid_date');

		if ($this->form_validation->run() === TRUE)
		{
			$plan_id = $this->input->post('plan_id');
			$start_date = $this->input->post('start');
			$expiry_date = $this->input->post('expiry');
			$username = $this->input->post('username');
			$status = $this->input->post('active');
			$amount = $this->input->post('amount');
			$payment_date = null;
			
			if ($this->input->post('payment'))
			{
				$payment_date = $this->input->post('payment');
			}
		}
		if ($this->form_validation->run() === TRUE)
		{
			$new_sub_id = $this->subs_model->create_subscription($plan_id, $start_date, $expiry_date, $username, $amount, $status, $payment_date);
			if ($new_sub_id)
			{
				// check to see if we are creating the plan
				// redirect them back to the admin page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth/subscriptions", 'refresh');
			}
			else
            {
				$this->session->set_flashdata('message', $this->ion_auth->errors());
            }
		}
			
		// display the create plan form
		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : ''));
		$modes = $this->plans_model->show_modes();
		// $this->data['plans'] = $plans;

		$this->data['all_modes'] = array('default' => 'Choose Mode');
		foreach ($modes as $mode):
			$this->data['all_modes'][$mode->id] = $mode->name;
		endforeach;

		$this->data['plan_id'] = [
			'name'  => 'plan_id',
			'id'    => 'plan_id',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('plan_id'),
		];
		$this->data['start'] = [
			'name'  => 'start',
			'id'    => 'start',
			'type'  => 'date',
			'value' => $this->form_validation->set_value('start'),
		];
		$this->data['expiry'] = [
			'name'  => 'expiry',
			'id'    => 'expiry',
			'type'  => 'date',
			'value' => $this->form_validation->set_value('expiry'),
		];
		$this->data['status'] = [
			'fields' => array(
				'0' => 'Unpaid',
				'1' => 'Paid'
			),
			'name' => 'active',
			'value' => $this->form_validation->set_value('status'),
		];
		$this->data['type'] = [
			'fields' => array(
				'0' => 'Cable',
				'1' => 'Internet'
			),
			'name' => 'type',
			'value' => $this->form_validation->set_value('type'),
		];
		$this->data['username'] = [
			'name'  => 'username',
			'id'    => 'username',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('username'),
		];
		$this->data['amount'] = [
			'name'  => 'amount',
			'id'    => 'amount',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('amount'),
		];
		$this->data['payment_date'] = [
			'name'  => 'payment',
			'id'    => 'payment',
			'type'  => 'date',
			'value' => $this->form_validation->set_value('payment_date'),
		];

		$this->load->view('templates/header');
		$this->load->view('auth/create/create_subscription', $this->data);
		$this->load->view('templates/footer');
	}

	// update subscription details
	public function manage($type, $id)
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin())
		{
			show_error('You do not have permission to view this page.');
		}

		$data = $this->subs_model->show($id);
		$plans = $this->plans_model->show_plans($id = null, $type);
		
		// pass the user to the view
		$this->data['data'] = $data;
		
		// validate form input
		$this->form_validation->set_rules('plan_id', 'Plan ID', 'trim|required|numeric');
		$this->form_validation->set_rules('start', 'Start date', 'trim|required|valid_date');
		$this->form_validation->set_rules('expiry', 'Expiry date', 'trim|required|valid_date');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required|numeric');
		$this->form_validation->set_rules('active', 'Status', 'trim|required|max_length[1]');
		$this->form_validation->set_rules('payment', 'Payment date', 'trim|required|valid_date');

		if ($this->form_validation->run() === TRUE)
		{
			$data = array(
				'plan_id' => $this->input->post('plan_id'),
				'start_date' => $this->input->post('start'),
				'expiry_date' => $this->input->post('expiry'),
				'status' => $this->input->post('active'),
				'amount' => $this->input->post('amount'),
				'payment_date' => $this->input->post('payment')
			);

			$response = $this->plans_model->update($data['plan_id'], $data, 'subscriptions');

			if ($response)
			{
				// redirect them back to the admin page if admin, or to the base url if non admin
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('auth/subscription/'.$this->data['data']->username);

			}
			else
			{
				// redirect them back to the admin page if admin, or to the base url if non admin
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				
				// redirect($this->agent->referrer());

			}
			return $response;
		}

		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
		$this->data['plans'] = $plans;
		$this->data['all_plans'] = array();
		foreach ($plans as $plan):
			$this->data['all_plans'][$plan->id] = $plan->plan;
		endforeach;
		
		$this->data['amount'] = [
			'name'  => 'amount',
			'id'    => 'amount',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('amount', $data->amount),
		];
		$this->data['start_date'] = [
			'name'  => 'start',
			'id'    => 'start',
			'type'  => 'date',
			'value' => $this->form_validation->set_value('start_date', $data->start_date),
		];
		$this->data['expiry_date'] = [
			'name'  => 'expiry',
			'id'    => 'expiry',
			'type'  => 'date',
			'value' => $this->form_validation->set_value('expiry_date', $data->expiry_date),
		];
		$this->data['payment_date'] = [
			'name'  => 'payment',
			'id'    => 'payment',
			'type'  => 'date',
			'value' => $this->form_validation->set_value('payment_date', $data->payment_date),
		];
		$this->data['status'] = [
			'fields' => array(
				'0' => 'Unpaid',
				'1' => 'Paid'
			),
			'name' => 'active',
			'value' => $this->form_validation->set_value('status', $data->status),
		];

		$this->load->view('templates/header');
		$this->load->view('auth/edit/edit_subscription', $this->data);
		$this->load->view('templates/footer');
	}

}