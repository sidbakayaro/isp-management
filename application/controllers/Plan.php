<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Plan extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url']);

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');

		$this->load->model('User');
		$this->load->model('Plans');

		$this->userModel = new User;
		$this->plans_model = new Plans;
	}

    public function show($id = null)
	{
		$response = $this->plans_model->show_plans($id);
		echo json_encode($response);
	}

	public function show_filtered($id)
	{
		$response = $this->plans_model->show_filtered_plans($id);
		echo json_encode($response);
	}

    public function create_mode()
	{

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		// validate form input
		$this->form_validation->set_rules('name', 'Mode name', 'trim|required|alpha_numeric_spaces');
		$this->form_validation->set_rules('active', 'Mode status', 'trim|required|numeric|max_length[1]');

		if ($this->form_validation->run() === TRUE)
		{
			$data = array(
				'name' => $this->input->post('name'),
				'status' => $this->input->post('active')
			);
			$new_mode_id = $this->plans_model->create_plan_mode($data);
			if ($new_mode_id)
			{
				// check to see if we are creating the mode
				// redirect them back to the modes page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth/modes", 'refresh');
			}
			else
            {
				$this->session->set_flashdata('message', 'Mode already exists.');
            }			
		}
			
		// display the create mode form
		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		$this->data['name'] = [
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('name'),
		];
		$this->data['active'] = [
			'fields' => array(
				'1' => 'Active',
				'0' => 'Inactive'
			),
			'name' => 'active',
			'value' => $this->form_validation->set_value('active'),
		];

		$this->load->view('templates/header');
		$this->load->view('auth/create/create_mode', $this->data);
		$this->load->view('templates/footer');
		
	}

	public function create()
	{

		// if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		// {
		// 	redirect('auth', 'refresh');
		// }

		// validate form input
		$this->form_validation->set_rules('plan_name', 'Plan name', 'trim|required|alpha_dash');
		$this->form_validation->set_rules('price', 'Plan price', 'trim|required|numeric');
		$this->form_validation->set_rules('company_price', 'Company price', 'trim|required|numeric');
		$this->form_validation->set_rules('active', 'Plan status', 'trim|required|numeric|max_length[1]');
		$this->form_validation->set_rules('mode_id', 'Plan mode', 'trim|required|numeric');
		$this->form_validation->set_rules('type', 'Plan type', 'trim|required|numeric');

		if ($this->form_validation->run() === TRUE)
		{
			$name = $this->input->post('plan_name');
			$price = $this->input->post('price');
			$company_price = $this->input->post('company_price');
			$status = $this->input->post('active');
			$mode_id = $this->input->post('mode_id');
			$type = $this->input->post('type');
		}
		if ($this->form_validation->run() === TRUE)
		{
			$new_plan_id = $this->plans_model->create_plan($name, $price, $company_price, $status, $mode_id, $type);
			if ($new_plan_id)
			{
				// check to see if we are creating the plan
				// redirect them back to the admin page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth/plans", 'refresh');
			}
			else
            {
				$this->session->set_flashdata('message', 'Plan already exists.');
            }
		}
			
		// display the create plan form
		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		$modes = $this->plans_model->show_modes();
		$this->data['all_modes'] = array('default' => 'Choose Mode');
		foreach ($modes as $mode):
			$this->data['all_modes'][$mode->id] = $mode->name;
		endforeach;

		$this->data['plan_name'] = [
			'name'  => 'plan_name',
			'id'    => 'plan_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('plan_name'),
		];
		$this->data['price'] = [
			'name'  => 'price',
			'id'    => 'price',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('price'),
		];
		$this->data['company_price'] = [
			'name'  => 'company_price',
			'id'    => 'company_price',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('company_price'),
		];
		$this->data['active'] = [
			'fields' => array(
				'1' => 'Active',
				'0' => 'Inactive'
			),
			'name' => 'active',
			'value' => $this->form_validation->set_value('active'),
		];
		$this->data['type'] = [
			'fields' => array(
				'0' => 'Cable',
				'1' => 'Internet'
			),
			'name' => 'type',
			'value' => $this->form_validation->set_value('type'),
		];

		$this->load->view('templates/header');
		$this->load->view('auth/create/create_plan', $this->data);
		$this->load->view('templates/footer');
		
	}

	public function manage($type, $id)
	{
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		$data = $this->plans_model->show_plans($id, $type);
		$modes = $this->plans_model->show_modes();
		
		// pass the user to the view
		$this->data['data'] = $data;
		
		// check inputs
		$this->form_validation->set_rules('plan', 'Plan name', 'trim|required|alpha_dash');
		$this->form_validation->set_rules('price', 'Plan price', 'trim|required|numeric');
		$this->form_validation->set_rules('company_price', 'Company price', 'trim|required|numeric');
		$this->form_validation->set_rules('active', 'Plan status', 'trim|required|numeric|max_length[1]');
		$this->form_validation->set_rules('mode', 'Plan mode', 'trim|required|numeric');

		if ($this->form_validation->run() === TRUE)
		{
			$data = array(
				'name' => $this->input->post('plan'),
				'price' => $this->input->post('price'),
				'company_price' => $this->input->post('company_price'),
				'status' => $this->input->post('active'),
				'mode_id' => $this->input->post('mode'),
			);
			
			$response = $this->plans_model->update($id, $data, 'plans');

			if ($response)
			{
				// redirect them back to the admin page if admin, or to the base url if non admin
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('auth/plans', 'refresh');

			}
			else
			{
				// redirect them back to the admin page if admin, or to the base url if non admin
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect($this->agent->referrer());

			}
			return $response;
		}

		$data = $data[0];

		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : ''));
		
		$this->data['all_modes'] = array();
		foreach ($modes as $mode):
			$this->data['all_modes'][$mode->id] = $mode->name;
			$mode->name === $data->mode ? $this->data['mode_id'] = $mode->id : '';
		endforeach;

		$this->data['name'] = [
			'name'  => 'plan',
			'id'    => 'name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('name', $data->plan),
		];
		$this->data['price'] = [
			'name'  => 'price',
			'id'    => 'price',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('price', $data->price),
		];
		$this->data['company_price'] = [
			'name'  => 'company_price',
			'id'    => 'company_price',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('company_price', $data->company_price),
		];
		$this->data['status'] = [
			'fields' => array(
				'0' => 'Inactive',
				'1' => 'Active'
			),
			'name' => 'active',
			'value' => $this->form_validation->set_value('status', $data->status),
		];
		$this->data['mode'] = [
			'name'  => 'mode',
			'id'    => 'mode',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('mode', $data->mode),
		];

		$this->load->view('templates/header');
		$this->load->view('auth/edit/edit_plan', $this->data);
		$this->load->view('templates/footer');
	}

	public function manage_mode($id)
	{
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		$data = $this->plans_model->show_modes($id);
		
		// check inputs
		$this->form_validation->set_rules('name', 'Mode name', 'trim|required|alpha_dash');
		$this->form_validation->set_rules('active', 'Plan status', 'trim|required|numeric|max_length[1]');

		if ($this->form_validation->run() === TRUE)
		{
			$data = array(
				'name' => $this->input->post('name'),
				'status' => $this->input->post('active')
			);
			$response = $this->plans_model->update($id, $data, 'plans_mode');

			if ($response)
			{
				// redirect them back to the admin page if admin, or to the base url if non admin
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('auth/modes', 'refresh');

			}
			else
			{
				// redirect them back to the admin page if admin, or to the base url if non admin
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect($this->agent->referrer());

			}
			return $response;
		}

		$data = $data[0];

		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : ''));

		$this->data['name'] = [
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('name', $data->name),
		];
		$this->data['status'] = [
			'fields' => array(
				'0' => 'Inactive',
				'1' => 'Active'
			),
			'name' => 'active',
			'value' => $this->form_validation->set_value('status', $data->status),
		];

		$this->load->view('templates/header');
		$this->load->view('auth/edit/edit_mode', $this->data);
		$this->load->view('templates/footer');
	}

	public function edit_mode($id)
	{
		// if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		// {
		// 	redirect('auth', 'refresh');
		// }
		
		// check inputs
		$this->form_validation->set_rules('name', 'Mode name', 'trim|required|alpha_dash');
		$this->form_validation->set_rules('active', 'Plan status', 'trim|required|numeric|max_length[1]');

		if ($this->form_validation->run() === TRUE)
		{
			$data = array(
				'name' => $this->input->post('name'),
				'status' => $this->input->post('active')
			);
			$response = $this->plans_model->update($id, $data, 'plans_mode');
			return $response;
		}

		echo validation_errors();
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
		echo $this->session->flashdata('message');
		$this->load->view('auth/create/create_plan_mode', $this->data);
	}

}